if (! "devtools" %in% row.names(installed.packages()))
  install.packages("devtools")

library(devtools)

if (! "isofor" %in% row.names(installed.packages()))
  install_github("Zelazny7/isofor")
library(isofor)

if (! "caret" %in% row.names(installed.packages()))
  install.packages("caret", repos="http://R-Forge.R-project.org")

library(caret)

if (! "pROC" %in% row.names(installed.packages()))
  install.packages("caret", repos="http://R-Forge.R-project.org")

library(pROC) #Receiver operating characteristic

# Load credit cards
creditcard <- read.csv("/home/pawel/mow/pr/src/creditcard.csv")

# Create copy of creditcard data in case of fast reloading data
creditcardCopy <- creditcard

# Copy data from backup
creditcard <- creditcardCopy

#standardyzacja atrybutu Amount
normalize <- function(x){
  return((x - mean(x, na.rm = TRUE))/sd(x, na.rm = TRUE))
}

# Normalization, should be omitted if done already
creditcard$Amount <- normalize(creditcard$Amount)


# Set Class as factor
set.seed(5627)
creditcard$Class <- as.factor(creditcard$Class)
levels(creditcard$Class) <- make.names(c("0","1"))

N<-nrow(creditcard)

ol = c(rep(0, N)) + 2

mod = iForest(X = creditcard, 100, 32)
p = predict(mod, creditcard)
col = ifelse(p > quantile(p, 0.95), "red", "blue")
plot(creditcard$V14, creditcard$V17, col=col)

#Preparing data for testing predictions
creditcard$PredictedClass <- ifelse(p > quantile(p, 0.95), 1 , 0)

# Set Class as factor
set.seed(5627)
creditcard$Class <- as.factor(creditcard$Class)
levels(creditcard$Class) <- make.names(c("0","1"))

creditcard$PredictedClass <- as.factor(creditcard$PredictedClass)

levels(creditcard$PredictedClass) <- make.names(c("0","1"))

# Test display
creditcard$Class
creditcard$PredictedClass

# Confusion matrix 
conf <- confusionMatrix(factor(creditcard$PredictedClass), factor(creditcard$Class), positive = "X0")
conf

# ROC 
ROC <- roc(creditcard$Class, as.numeric(creditcard$PredictedClass))
plot(ROC, col = "blue")
title("ROC curve for Isolation Forest")
auc(ROC)