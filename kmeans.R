# Load credit cards
creditcard <- read.csv("C:/Users/Marcin/Desktop/creditcard.csv")

# Create copy of creditcard data in case of fast reloading data
creditcardCopy <- creditcard

# Copy data from backup
creditcard <- creditcardCopy

# Normalization, should be omitted if done already
creditcard$Amount <- normalize(creditcard$Amount)

# Set Class as factor
set.seed(5639)
creditcard$Class <- as.factor(creditcard$Class)
levels(creditcard$Class) <- make.names(c("0","1"))

# Perform kmeans with 2 clusters and add predictions to creditcard
k <- kmeans(creditcard[,2:30], 2)
creditcard$PredictedClass <- k$cluster

# Visualize clusters
fviz_cluster(k, creditcard[,2:30])

# Prepare data for confusion matrix
creditcard$PredictedClass <- as.factor(creditcard$PredictedClass)

levels(creditcard$PredictedClass) <- make.names(c("0","1"))

# Test display
#creditcard$Class
#creditcard$PredictedClass

# Confusion matrix 
conf <- confusionMatrix(factor(creditcard$PredictedClass), factor(creditcard$Class), positive = "x0")
conf

# ROC 
ROC <- roc(creditcard$Class, as.numeric(creditcard$PredictedClass))
plot(ROC, col = "blue")
auc(ROC)
